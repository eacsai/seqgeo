import argparse
import os
import numpy as np
import math
import itertools
from torch.utils.tensorboard import SummaryWriter

from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F

from model import SatelliteFeatureExtractor, StreetFeatureExtractor, TransMixer
from KITTI_dataset import *
from SMTL import softMarginTripletLoss

STREET_IMG_WIDTH = 320
STREET_IMG_HEIGHT = 180
SATELLITE_IMG_WIDTH = 256
SATELLITE_IMG_HEIGHT = 256
SEQUENCE_SIZE = 4


def ValidateOne(distArray, topK):
    acc = 0.0
    dataAmount = 0.0
    for i in range(distArray.shape[0]):
        groundTruths = distArray[i, i]
        pred = torch.sum(distArray[:, i] < groundTruths)
        if pred < topK:
            acc += 1.0
        dataAmount += 1.0
    return acc / dataAmount


def ValidateAll(streetFeatures, satelliteFeatures):
    distArray = 2 - 2 * \
        torch.matmul(satelliteFeatures, torch.transpose(streetFeatures, 0, 1))
    topOnePercent = int(distArray.shape[0] * 0.01) + 1
    valAcc = torch.zeros((1, topOnePercent))
    for i in range(topOnePercent):
        valAcc[0, i] = ValidateOne(distArray, i)

    return valAcc


class LambdaLR():
    def __init__(self, n_epochs, offset, decay_start_epoch):
        '''
        linear decay LR scheduler
        n_epochs: number of total training epochs
        offset: train start epochs
        decay_start_epoch: epoch start decay
        '''
        assert ((n_epochs - decay_start_epoch) >
                0), "Decay must start before the training session ends!"
        self.n_epochs = n_epochs
        self.offset = offset
        self.decay_start_epoch = decay_start_epoch

    def step(self, epoch):
        return 1.0 - max(0, epoch + self.offset - self.decay_start_epoch)/(self.n_epochs - self.decay_start_epoch)


def save_model(savePath, transMixer, sateFeature, strFeature, epoch):
    modelFolder = os.path.join(savePath, f"epoch_{epoch}")
    os.makedirs(modelFolder)
    torch.save(transMixer.state_dict(), os.path.join(
        modelFolder, f'trans_{epoch}.pth'))
    torch.save(sateFeature.state_dict(), os.path.join(
        modelFolder, f'SFE_{epoch}.pth'))
    torch.save(strFeature.state_dict(), os.path.join(
        modelFolder, f'GFE_{epoch}.pth'))
    # torch.save(HPEstimator.state_dict(), os.path.join(modelFolder, f'HPE_{epoch}.pth'))


def InferOnce(grdFE, satFE, transMixer, batch, device, noMask):
    grdImgs = batch[1].to(device)
    sateImgs = batch[0].to(device).unsqueeze(1)

    numSeqInBatch = grdImgs.shape[0]

    # street view featuer extraction
    grdImgs = grdImgs.view(grdImgs.shape[0]*grdImgs.shape[1],
                           grdImgs.shape[2], grdImgs.shape[3], grdImgs.shape[4])

    grdFeature = grdFE(grdImgs)
    grdFeature = grdFeature.view(numSeqInBatch, SEQUENCE_SIZE, -1)

    # satellite view feature extraction
    sateImgs = sateImgs.view(sateImgs.shape[0], sateImgs.shape[1]*sateImgs.shape[2],
                             sateImgs.shape[3], sateImgs.shape[4])
    sateFeature = satFE(sateImgs)
    sateFeature = sateFeature.view(numSeqInBatch, -1)
    # print(sateFeature.shape)

    if not noMask:
        grdMixedFeature = transMixer(grdFeature, masked_range=[
                                     0, 6], max_masked=opt.max_masked)
    else:
        grdMixedFeature = transMixer(
            grdFeature, max_masked=0, masked_range=[0, 6])
    grdGlobalFeature = grdMixedFeature.permute(0, 2, 1)
    grdGlobalLatent = F.avg_pool1d(
        grdGlobalFeature, grdGlobalFeature.shape[2]).squeeze(2)

    return sateFeature, grdGlobalLatent


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--n_epochs", type=int, default=50,
                        help="number of epochs of training")
    parser.add_argument("--epoch", type=int, default=0,
                        help="number of epochs start training")
    parser.add_argument("--decay_epoch", type=int, default=30,
                        help="number of epochs start decaying LR")
    parser.add_argument("--batch_size", type=int,
                        default=24, help="size of the batches")
    parser.add_argument("--lr", type=float, default=1e-5, help="learning rate")
    parser.add_argument("--save_name", type=str,
                        default='SAVE_NAME', help='name of the model')
    parser.add_argument("--feature_dims", type=int,
                        default=4096, help="latent feature dimension")
    parser.add_argument("--backbone", type=str,
                        default="vgg16", help='weight for heading loss')
    parser.add_argument("--beta1", type=float,
                        default=0.9, help='beta1 for adam')
    parser.add_argument("--beta2", type=float,
                        default=0.999, help='beta2 for adam')
    parser.add_argument("--num_workers", type=int,
                        default=12, help='num of CPUs')
    parser.add_argument("--lambda_SMTL", type=float,
                        default=1.0, help='weight for triplet loss')
    parser.add_argument("--gamma", type=float, default=10.0,
                        help='value for SMTL gamma')
    parser.add_argument("--weight_decay", type=float,
                        default=1e-2, help='value for SMTL gamma')
    parser.add_argument('--no_mask', default=True, action='store_true')
    parser.add_argument('--MHA_layers', type=int, default=6,
                        help="number of MHA layers")
    parser.add_argument('--max_masked', type=int,
                        default=6, help="max masked frames")
    parser.add_argument('--nHeads', type=int, default=8,
                        help="number of heads")

    parser.add_argument('--gpu_ids', type=str, default='0',
                        help='gpu ids: e.g. 0  0,1,2, 0,2. use -1 for CPU')

    parser.add_argument('--sequence', type=int, default=4,
                        help='img size of sequence')
    opt = parser.parse_args()
    print(opt)
    gpu_ids = list(map(int, opt.gpu_ids.split(',')))
    zoom = 20
    length = opt.sequence
    print(f"zoom level:{zoom}")
    print("sequence length : ", length)

    #saving path for training logs
    writer = SummaryWriter(opt.save_name)
    savePath=os.path.join(opt.save_name, 'training_logs')
    if not os.path.exists(savePath):
        os.makedirs(savePath)
    else:
        print("Note! Saving path existed !")

    #set device
    device = torch.device("cuda:{0}".format(gpu_ids[0]) if torch.cuda.is_available() else "cpu")
    print("Using device:",device)

    transMixer = TransMixer(transDimension=opt.feature_dims, max_length=length, numLayers = opt.MHA_layers, nHead=opt.nHeads)
    

    grdFeatureExtractor = StreetFeatureExtractor(backbone = opt.backbone)
    satelliteFeatureExtractor = SatelliteFeatureExtractor(backbone = opt.backbone, inputChannel=3)
    
    if len(gpu_ids) > 0:
        assert(torch.cuda.is_available())
        transMixer.to(gpu_ids[0])
        transMixer = nn.DataParallel(transMixer, gpu_ids)  # multi-GPUs
        grdFeatureExtractor.to(gpu_ids[0])
        grdFeatureExtractor = nn.DataParallel(grdFeatureExtractor, gpu_ids)  # multi-GPUs
        satelliteFeatureExtractor.to(gpu_ids[0])
        satelliteFeatureExtractor = nn.DataParallel(satelliteFeatureExtractor, gpu_ids)  # multi-GPUs

    # Optimizers
    optimizer = torch.optim.Adam(itertools.chain(transMixer.parameters(), grdFeatureExtractor.parameters(
    ), satelliteFeatureExtractor.parameters()), lr=opt.lr, betas=(opt.beta1, opt.beta2), weight_decay=1e-6)
    lrSchedule = torch.optim.lr_scheduler.LambdaLR(
        optimizer, lr_lambda=LambdaLR(opt.n_epochs, opt.epoch, opt.decay_epoch).step)

    mini_batch = opt.batch_size
    trainloader = load_data(train_file, mini_batch, 0, length,
                            0, 0, use_project_grd=0,
                            use_semantic=0)
    valloader = load_data(val_file, mini_batch, 0, length,
                            0, 0, use_project_grd=0,
                            use_semantic=0, mode='val')

    # training
    allLosses = []
    print("start training...")
    for epoch in range(opt.n_epochs):
        # set the model to train mode
        transMixer.train()
        grdFeatureExtractor.train()
        satelliteFeatureExtractor.train()

        epochLoss = 0
        epochTripletLoss = 0

        for batch in tqdm(trainloader, disable=False):
            sat_map, grd_left_imgs, heading_array = [item.cuda() for item in batch]
            if grd_left_imgs.shape[0] < 2:
                continue

            sateFeature, grdGlobalLatent =\
                InferOnce(grdFeatureExtractor,
                          satelliteFeatureExtractor,
                          transMixer, batch, device, opt.no_mask)

            # softmargin triplet loss
            sateFeatureUnit = sateFeature / \
                torch.linalg.norm(sateFeature, dim=1, keepdim=True)
            grdGlobalLatentUnit = grdGlobalLatent / \
                torch.linalg.norm(grdGlobalLatent, dim=1, keepdim=True)

            lossTriplet = softMarginTripletLoss(
                sateFeatureUnit, grdGlobalLatentUnit, opt.gamma)

            loss = opt.lambda_SMTL * lossTriplet
            # optimize
            optimizer.zero_grad()
            loss.backward()
            # torch.nn.utils.clip_grad_norm_(itertools.chain(transMixer.parameters(),grdFeatureExtractor.parameters(), satelliteFeatureExtractor.parameters()),  1.0)
            optimizer.step()

            epochLoss += loss.item()
            epochTripletLoss += lossTriplet.item()
        # step learning rate
        lrSchedule.step()

        # calculate epoch average loss
        epochLoss = float(epochLoss) / float(len(trainloader))
        epochTripletLoss = float(epochTripletLoss) / float(len(trainloader))

        # add to all losses list
        allLosses.append(epochLoss)
        if epoch % 10 == 9:
            save_model(savePath, transMixer, satelliteFeatureExtractor,
                       grdFeatureExtractor, epoch+1)

        # set the model to evaluate mode
        transMixer.eval()
        grdFeatureExtractor.eval()
        satelliteFeatureExtractor.eval()

        valSateFeatures = None
        valStreetFeature = None

        with torch.no_grad():
            for batch in tqdm(valloader, disable=False):

                sateFeature, grdGlobalLatent =\
                    InferOnce(grdFeatureExtractor,
                              satelliteFeatureExtractor,
                              transMixer, batch, device, opt.no_mask)

                # softmargin triplet loss
                sateFeatureUnit = sateFeature / \
                    torch.linalg.norm(sateFeature, dim=1, keepdim=True)
                grdGlobalRespresentUnit = grdGlobalLatent / \
                    torch.linalg.norm(grdGlobalLatent, dim=1, keepdim=True)

                # stack features to the container
                if valSateFeatures == None:
                    valSateFeatures = sateFeatureUnit.detach()
                else:
                    valSateFeatures = torch.cat(
                        (valSateFeatures, sateFeatureUnit.detach()), dim=0)

                if valStreetFeature == None:
                    valStreetFeature = grdGlobalRespresentUnit.detach()
                else:
                    valStreetFeature = torch.cat(
                        (valStreetFeature, grdGlobalRespresentUnit.detach()), dim=0)

            # Retrival accuracy
            valAcc = ValidateAll(valStreetFeature, valSateFeatures)
            print(
                f"==============Summary of epoch {epoch} on validation set=================")
            try:
                # print epoch loss
                print("---------loss---------")
                print(f"Epoch {epoch} Loss {epochLoss}")
                print(f"triplet loss:{epochTripletLoss}")
                writer.add_scalars('losses', {
                    'epoch_loss': epochLoss,
                    'triplet_loss': epochTripletLoss
                }, epoch)
                print("----------------------")
                print('top1', ':', valAcc[0, 1] * 100.0)
                print('top5', ':', valAcc[0, 5] * 100.0)
                print('top10', ':', valAcc[0, 10] * 100.0)
                print('top1%', ':', valAcc[0, -1] * 100.0)
                writer.add_scalars('validation recall@k', {
                    'top 1': valAcc[0, 1],
                    'top 5': valAcc[0, 5],
                    'top 10': valAcc[0, 10],
                    'top 1%': valAcc[0, -1]
                }, epoch)
            except:
                print(valAcc)

            print(
                "========================================================================")

    writer.close()  # close tensorboard
